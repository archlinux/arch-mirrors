from .args import subparsers, load_arguments
from .arguments.build import parse_build
from .arguments.verify import parse_verify

loaded = True