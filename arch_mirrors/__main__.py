import importlib
import sys
import pathlib

# Load .git version before the builtin version
if pathlib.Path('./_tooling/__init__.py').absolute().exists():
	spec = importlib.util.spec_from_file_location("arch_mirrors", "./_tooling/__init__.py")
	arch_mirrors = importlib.util.module_from_spec(spec)
	sys.modules["arch_mirrors"] = arch_mirrors
	spec.loader.exec_module(sys.modules["arch_mirrors"])
else:
	import arch_mirrors

if __name__ == '__main__':
	arch_mirrors.run_as_a_module()