from enum import Enum
from pydantic import BaseModel, Field
from typing import List


class VersionV1(BaseModel):
	name: str
	tier :int = Field(ge=0, le=3)
	bandwidth :float # In Gb/s
	https :List[str] # TODO: Validate (after we got Pydantic2, not worth the migration hassle)
	http :List[str] # TODO: Validate (after we got Pydantic2, not worth the migration hassle)
	rsync :List[str] # TODO: Validate (after we got Pydantic2, not worth the migration hassle)
	upstream :str|None = None # TODO: After all mirrors are loaded, verify that the reference exist, and if None verify that it's a Tier1
	IP :List[str]|None = None # TODO: Validate IPv4 & IPv6 (after we got Pydantic2, not worth the migration hassle)
	visible :bool = True

	def __init__(self, **initial_data):
		name = set(initial_data.keys()) - {'compatability'} or [initial_data.get('name')]

		if not name:
			raise KeyError(f"Mirror listing VersionV1 requires a name")

		# To avoid multiple named parameters in __init__()
		if initial_data.get('name'):
			del(initial_data['name'])

		name = name.pop()

		super().__init__(name=name, **initial_data[name])


class VersionParsers(Enum):
	maximum = VersionV1
	v1 = VersionV1

	@classmethod
	def list(cls):
		return list(map(lambda c: c.value, cls))