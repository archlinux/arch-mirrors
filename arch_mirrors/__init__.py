from .cli.args_router import loaded
from .cli.args import load_arguments

__version__ = "0.1"

def run_as_a_module():
	from .session import session
	session['args'] = load_arguments()
	session['args'].func(session['args'])